 ![](docs/logo.png)
# Cyphers Stock Market Simulator Game

# Hosted URL: https://cyphers-smsg.firebaseapp.com/ #


## Team members

| UCD Student No. | Name                                | Gitlab ID                   |
| --------------- | ----------------------------------- | --------------------------- |
| 16211142        | Shan Indrajith Wijenayaka           | Shan I. Wijenayake/shanwije |
| 16211163        | G. G. L. Thilanka Bandara Godamunna | Lakmal Thilanka             |
| 16211164        | Hasini Anupama Nagahawatte          | anupama                     |
| 16211165        | Pramod Chanaka W. Kulathunga        | Pramod Weerasinghe          |
| 16211187        | T. P. Nadeeshani Silva              | Priyanjala                  |
| 16211216        | W. P. Kalpana Fernando              | Kalpana Fernando/Unknown    |

## Folder structure

├── backend  
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└── StockSim - Includes the server side implementation, ie. the game logic of the application  
├── docs - Includes all documentation  
└── front - Includes the Angular 6 web app; the UI of the application  

# Building and running the project

## System requirements

-  Java SE Development Kit 8 http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
-  Node.js 10.5.0 https://nodejs.org/en/download/current/
-  Maven 3.5.4 https://maven.apache.org/download.cgi
-  Netbeans 8.0.2 http://download.netbeans.org/netbeans/8.0.2/final/

The frontend and the backend should be built individually. So it's easy to clone (or download) the project at the root level. 

To clone using Git, run:

```console
git clone https://gitlab.com/cyphers/stock-market-simulation-game.git
```



## Building the frontend

The frontend project is contained within the `frontend` folder. This is a standard Angular 6 application based on the Angular CLI.

**Build instructions:**

Make sure that Node.js is installed. To verify run `node --version`. If Node.js is installed, the command will print the installed version.

```console
$ node --version
v10.4.1
```

1. Navigate to `\front`

2. Within the `front` folder, open a Command Prompt (Shift + Right Click --> Open Command Window Here)

3. Install all the required Node.js dependencies by running `npm i`
```console
$ npm i
   ```
Make sure the computer is connected to the internet.

4. Install the Angular CLI by running `npm install -g @angular/cli`
```console
$ npm install -g @angular/cli
```
This will install the Angular command line tools allowing you to run an Angular app.

5. Once the installation is complete, run `ng serve` to compile and run the Angular application
```console
$ ng serve
```
6. On your browser go to `http://localhost:4200/` to launch the application. (A modern browser like Chrome is preferred)

   
## Building the backend

   The backend project is contained within the `backend\StockSim` folder. It is a standard Maven project. Since we developed this project on Netbeans, it would be easy to build it using Netbeans.

   

   We are using AWS to to deploy the backend project. So we can not run this as a local server. Although this may sound tedious, this made backend/frontend integration much easier since the frontend is always communicating with the latest backend.

   You will find more instructions on how to deploy to AWS at the latter part of this document.

   

   **Build instructions:**

   Make sure you have Java 8 and Maven installed. To verify run `javac -version` and `mvn -v` respectively. If they are are properly installed, you will see the following output.

   ```console
   $ javac -version
   javac 1.8.0_172
   
   $ mvn -v
   Apache Maven 3.5.3 (3383c37e1f9e9b3bc3df5050c29c8aff9f295297; 2018-02-25T01:19:05+05:30)
   Maven home: C:\programs\apache-maven-3.5.3\bin\..
   Java version: 1.8.0_172, vendor: Oracle Corporation
   Java home: C:\Program Files\Java\jdk1.8.0_172\jre
   Default locale: en_US, platform encoding: Cp1252
   OS name: "windows 10", version: "10.0", arch: "amd64", family: "windows"
   ```


1. To import the project into Netbeans :
   - File --> Open Project
   - Navigate to `\backend` folder
   - Select `StockSim` (which should have the Maven project icon)
2. Netbeans may indicate that the project has problems. This is happening since the Maven dependencies are not downloaded. To fix this:
   - Right click on the project in the Project pane
   - Click "Resolve Project Problems"
   - In the resulting dialog, press "Resolve". This will download all missing dependencies automatically
3. Now we need to generate a .JAR file with the dependencies. To generate this file:
   - Right click on the project in the Project pane
   - Click "Build with Dependencies"
   - The .JAR file will be generated in the `.\StockSim\target` folder as `StockSim-1.0-jar-with-dependencies.jar`
   - Remember the location of this file since we will be needing this to upload to AWS

## Deploying the Server

We host the backend on AWS as a AWS Lambda function. By doing this AWS will expose a set of APIs which the frontend can use to access the game logic.

**Deployment instructions**

We have a set of credentials provided with enough access to upload the .JAR file you built in the previous section. Simply uploading the .JAR file is sufficient since AWS will automatically configure everything else.

| AWS Property  | Value                                                  |
|---------------|--------------------------------------------------------|
| URL           | https://iamcipher.signin.aws.amazon.com/console/lambda |
| IAM user name | cipher                                                 |
| Password      | Ciphers@123                                            |
<br>
1. Login to AWS using above URL and credentials and you will see a dashboard
   
    ![](docs/aws_lambda.png)

2. Click on `ciphersFunction` and you'll navigate to another dashboard
   
    ![](docs/aws_func.png)

3. Click "Upload" and you will be prompted to select a file. Go to `.\StockSim\target` and select `StockSim-1.0-jar-with-dependencies.jar` 

4. The "Save" button on the top right corner will get activated (Orange color), and click "Save". You'll notice a spinning indicator within the "Save" button. Once this stops and the "Save" button appears disabled, the .JAR is uploaded and ready (If you expand the "Designer" panel, you will see another status indicator saying "Saved").

5. On the browser open our locally running Angular frontend (`http://localhost:4200/`) and hit Refresh. The frontend will automatically connect to the AWS hosted backend and allow you to run the application.



