/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apiCall;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

/**
 *
 * @author Shan Wijenayaka
 */
public class MainHandler implements RequestHandler<MainInput, MainOutput> {

    @Override
    public MainOutput handleRequest(MainInput mainInput, Context cntxt) {
        MainOutput mo = new MainOutput();
        mo.setMsg("Hello from ciphers, " + mainInput.getName());
        mo.setRes(mainInput.getController(), mainInput.getData(), mainInput.getUserID());
        mo.setUserID(mainInput.getUserID());
        mo.setFunctionName(cntxt.getFunctionName());
        mo.setMemoryLimit(cntxt.getMemoryLimitInMB());
        cntxt.getLogger().log(mainInput.getController());
        return mo;
    }

}
