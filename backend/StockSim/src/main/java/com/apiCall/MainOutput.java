/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apiCall;

import com.ctrl.GameController;
import com.game.AllGames;
import com.test.SetUpGame;
import java.util.ArrayList;

/**
 *
 * @author Shan Wijenayaka
 */
public class MainOutput {

    String msg;
    String functionName;
    String userID;
    int memoryLimit;
    Object res;

//    use the below main function for API testings locally
    public static void main(String[] args) {
        MainOutput response = new MainOutput();
        ArrayList<String> requestData = new ArrayList<>();

        requestData.add("shan");
        String requestCtrl = "createNewGame";
        try {
            response.setRes(requestCtrl, requestData, "shan");
        } finally {
            System.out.println("response to the front end is , " + response.getRes());
        }

    }

    public Object getRes() {
        return res;
    }

    public void setRes(String controller, ArrayList<String> data, String userID) {

        switch (controller) {
            //include your API identifier and related return function here - shan
            case "setGame":
                this.res = new SetUpGame().welcomeGame(data);
                break;
            case "createNewGame":
                this.res = GameController.getGameController().createNewGame(data);
                break;
            case "joinPlayerToAGame":
                this.res = GameController.getGameController().joinPlayerToGame(data, userID);
                break;
            case "getPlayerAddedStatus":
                this.res = GameController.getGameController().getStatusOfPlayers(data, userID);
                break;
            case "buy":
                if (userID.isEmpty() && AllGames.getGameForKey(userID) == null) {
                    this.res = "UserID is ivalid or there is no game for the given userID";
                    break;
                }
                // Data needed:
                //  PlayerID, stockSymbol (data[0]), amount (data[1]), stockPrice(data[2]), turn(data[3])
                // return Status object
                String uIDbuy = data.get(0);
                this.res = AllGames.getGameForKey(uIDbuy).buy(uIDbuy, data.get(1), Integer.valueOf(data.get(2)), Integer.valueOf(data.get(3)), Integer.valueOf(data.get(4)));
                break;
            case "sell":
                if (userID.isEmpty() && AllGames.getGameForKey(userID) == null) {
                    this.res = "UserID is ivalid or there is no game for the given userID";
                    break;
                }
                // Data needed:
                //  PlayerID, stockSymbol (data[0]), amount (data[1]), stockPrice(data[2]), turn(data[3])
                // return Status object
                String uIDsell = data.get(0);
                this.res = AllGames.getGameForKey(uIDsell).sell(uIDsell, data.get(1), Integer.valueOf(data.get(2)), Integer.valueOf(data.get(3)), Integer.valueOf(data.get(4)));
                break;
            case "initializeDashboard":
                //This should only be called the first time the dashboard loads.
                this.res = AllGames.getGameForKey(userID).getDashboard();
                break;
            case "getTurnStatus":
                this.res = AllGames.getGameForKey(userID).getTurnStatus(Integer.valueOf(data.get(0)));
                break;
            case "setPlayerTurnOver":
                this.res = AllGames.getGameForKey(userID).setPlayerTurnOver(userID);
                break;
            case "getDashboardForCurrentTurn":
                this.res = AllGames.getGameForKey(userID).getDashboardForCurrentTurn();
                break;
            case "getPlayerStaus":
                this.res = AllGames.getGameForKey(userID).getPlayerStaus(userID);
                break;
            default:
                this.res = "can't identify the controller : request : " + data;
        }
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public int getMemoryLimit() {
        return memoryLimit;
    }

    public void setMemoryLimit(int memoryLimit) {
        this.memoryLimit = memoryLimit;
    }

    /**
     * @return the userID
     */
    public String getUserID() {
        return userID;
    }

    /**
     * @param userID the userID to set
     */
    public void setUserID(String userID) {
        this.userID = userID;
    }

}
