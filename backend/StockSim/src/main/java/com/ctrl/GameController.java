package com.ctrl;

import com.ctrl.returnObjects.JoinPlayerToGameReturn;
import com.game.AllGames;
import com.game.Game;
import com.game.Player;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Shan Wijenayaka
 */
public class GameController {

    private GameController() {

    }
    private static final GameController gc = new GameController();

    public static GameController getGameController() {
        return gc;
    }

    public Object createNewGame(ArrayList<String> data) {
        if (data.get(0).length() > 0) {
            String[] gameKeys = Game.generateKeys();
            Player p = new Player(data.get(0), gameKeys[0]); //username
            Game g = new Game(p.getUserName(), p, gameKeys);
            p.setAssociatedGame(g);
            String[] remainingKeys = new String[gameKeys.length - 1]; // first key assign to creator
            AllGames.addGame(g);

            //this invalidate game after 45mins
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    AllGames.removeGame(g);
                }
            }, 45 * 60 * 1000);

            return gameKeys;
        } else {
            return "Error : please provide a proper username";
        }

    }

    public Object joinPlayerToGame(ArrayList<String> data, String userID) {
        String key = userID;
        String PlayerName = data.get(0);
        Game AssociatedGame = AllGames.getGameForKey(key);
        if (AssociatedGame != null) {
            Player p = new Player(PlayerName, userID);
            p.setAssociatedGame(AssociatedGame);
            boolean isKeyAlreadyNotInGame = AssociatedGame.addPlayerToMap(key, p);

            if (isKeyAlreadyNotInGame) {
                String creator = AssociatedGame.getCreatorName();
                ArrayList<String> returnList = new ArrayList<>();
                JoinPlayerToGameReturn returnObj = new JoinPlayerToGameReturn();
                returnObj.setCreatorName(creator);
                returnObj.setCreatorID(AssociatedGame.getCreatorID());
                returnObj.setStatus("Success");
                returnObj.setPlayerList(AssociatedGame.getAllPlayerNameList());
                return returnObj;
            } else {
                JoinPlayerToGameReturn returnObj = new JoinPlayerToGameReturn();
                returnObj.setStatus("Error");
                returnObj.setMsg("Key is already assoicated with a game");
                return returnObj;
            }

        } else {
            JoinPlayerToGameReturn returnObj = new JoinPlayerToGameReturn();
            returnObj.setStatus("Error");
            returnObj.setMsg("Error : invalid key, please try again");
            return returnObj;
        }
    }

    public Object getStatusOfPlayers(ArrayList<String> data, String userID) {
        ArrayList<Game> games = AllGames.getGames();
        if (0 < userID.length()) {
            for (Game g : games) {
                if (g.getCreatorID().equals(userID)) {
                    ArrayList<String[]> playerList =  g.getAllPlayerNameList();
                    if(playerList.size() == Game.getKEYCOUNT()){
                        return "START_GAME";
                    } else {
                        return playerList;
                    }
                }
            }
            return "Error : Given ID is not associated with any Game";
        } else {
            return "Error : Please provide a user ID :" + userID;
        }

    }

    public Object clearGame(ArrayList<String> data) {
        boolean isSuccess = AllGames.removeGameByCreatorID(data.get(0));
        return isSuccess;
    }

}
