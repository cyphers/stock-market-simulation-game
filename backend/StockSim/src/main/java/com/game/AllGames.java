/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.game;

import java.util.ArrayList;

/**
 *
 * @author Shan Wijenayaka
 */
public class AllGames {

    private static final AllGames AllGamesObj = new AllGames();
    private static final ArrayList<Game> games = new ArrayList<>();

    /**
     * @return the games
     */
    public static ArrayList<Game> getGames() {
        return games;
    }

    /**
     * @param g
     */
    public static void addGame(Game g) {
        games.add(g);
    }

    public static void removeGame(Game g) {
        if (games.contains(g)) {
            games.remove(g);
        } else {
            System.out.println("game is not in games array");
        }
    }

    public static boolean removeGameByCreatorID(String ID) {
        for (Game game : AllGames.getGames()) {
            if (game.getCreatorID() == null ? ID == null : game.getCreatorID().equals(ID)) {
                AllGames.removeGame(game);
                return true;
            }
        }
        return false;
    }

    public static Game getGameForKey(String key) {
        Game theGame = null;
        ArrayList<Game> games = AllGames.getGames();
        for (Game g : games) {
            String[] keys = g.getKeys();
            for (String k : keys) {
                if (k.equals(key)) {
                    theGame = g;
                    return theGame;
                }
            }
        }
        return theGame;
    }

    public static Player findPlayerByPlayerID(String playerID){
        return  AllGames.getGameForKey(playerID).getPlayerMap().get(playerID);
    }

}
