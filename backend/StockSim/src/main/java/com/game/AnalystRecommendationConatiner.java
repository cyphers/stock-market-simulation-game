package com.game;

import cyphers.analyst.AnalystRecommendation;

public class AnalystRecommendationConatiner {

    private String company;
    private AnalystRecommendation recommendation;

    public AnalystRecommendationConatiner(String company, AnalystRecommendation recommendation) {
        this.company = company;
        this.recommendation = recommendation;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public AnalystRecommendation getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(AnalystRecommendation recommendation) {
        this.recommendation = recommendation;
    }

}
