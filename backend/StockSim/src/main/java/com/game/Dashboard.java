package com.game;

import cyphers.company.Company;
import java.util.ArrayList;
import java.util.List;

public class Dashboard {

    private boolean gameStatus;
    private List<Company> companies;
    private List<List<Integer>> chartInfo;
    private List<PlayerInfo> playerInfo;
    private int currentTurn = 0;
    private String event;
    private String eventType;
    private String eventPrediction = "";
    private List<StockPortfolioContainer> stockPortfolioList = new ArrayList<>();
    private List<AnalystRecommendationConatiner> analystRecommendationList = new ArrayList<>();

    public Dashboard(List<Company> companies, List<List<Integer>> chartInfo, List<PlayerInfo> playerInfo) {
        this.companies = companies;
        this.chartInfo = chartInfo;
        this.playerInfo = playerInfo;
    }

    public List<AnalystRecommendationConatiner> getAnalystRecommendationList() {
        return analystRecommendationList;
    }

    public void setAnalystRecommendationList(List<AnalystRecommendationConatiner> analystRecommendationList) {
        this.analystRecommendationList = analystRecommendationList;
    }

    public List<StockPortfolioContainer> getStockPortfolioList() {
        return stockPortfolioList;
    }

    public void setStockPortfolioList(List<StockPortfolioContainer> stockPortfolioList) {
        this.stockPortfolioList = stockPortfolioList;
    }

    public boolean isGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(boolean gameStatus) {
        this.gameStatus = gameStatus;
    }

    public String getEventPrediction() {
        return eventPrediction;
    }

    public void setEventPrediction(String eventPrediction) {
        this.eventPrediction = eventPrediction;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public int getCurrentTurn() {
        return currentTurn;
    }

    public void setCurrentTurn(int currentTurn) {
        this.currentTurn = currentTurn;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }

    public List<List<Integer>> getChartInfo() {
        return chartInfo;
    }

    public void setChartInfo(List<List<Integer>> chartInfo) {
        this.chartInfo = chartInfo;
    }

    public List<PlayerInfo> getPlayerInfo() {
        return playerInfo;
    }

    public void setPlayerInfo(List<PlayerInfo> playerInfo) {
        this.playerInfo = playerInfo;
    }
}
