/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.game;

import com.utils.Utils;
import cyphers.analyst.Analyst;
import cyphers.analyst.AnalystRecommendation;
import cyphers.bank.Bank;
import cyphers.bank.BankAccount;
import cyphers.broker.Broker;
import cyphers.broker.StockAccount;
import cyphers.broker.StockEntry;
import cyphers.broker.StockTransaction;
import cyphers.company.Company;
import cyphers.company.CompanyCollection;
import cyphers.simulator.EventStream;
import cyphers.simulator.Trends;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Shan Wijenayaka
 */
public class Game {
//<editor-fold defaultstate="collapsed" desc="init_imp">

    public static int getKEYCOUNT() {
        return KEYCOUNT;
    }

    private final String creatorID;
    private final String creatorName;
    private final String gameID;
    private final HashMap<String, Player> playerMap;
    private final Bank bank = new Bank();
    private final Broker broker = new Broker(bank);
    private final String[] keys;

    static final int KEYCOUNT = 2;

    private final int[] randomTrend = Trends.getRandomTrend();
    private final int[] sectorTrend = Trends.getTrend();
    private final int[] marketTrend = Trends.getTrend();
    private final EventStream eventStream = Trends.getEventStream();
    private final List<Company> companies = CompanyCollection.getCompanies();
    private List<String> statusIndicator = new ArrayList<>();
    private int currentTurn = 14;
    Dashboard initialDashboard;

    public Game(String creatorID, Player p, String[] keys) {

        this.keys = keys;
        this.creatorID = keys[0];
        this.creatorName = p.getUserName();
        this.gameID = "game_" + keys[0];

        this.playerMap = new HashMap<>(); // highly important, Each player userID with playerObject reference
        this.playerMap.put(this.creatorID, p); // adding the creator as a player to playermap

        // Bank account should be created before the broker account;
        bank.createAccount(p.getUserID());
        try {
            broker.createAccount(p.getUserID());
        } catch (Exception ex) {
            // Either handle the exception here, or the place where Game objects are created.
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String[] generateKeys() {
        String keys[] = new String[KEYCOUNT];
        for (int i = 0; i < keys.length; i++) {
            String key = Utils.generateKey();
            System.out.println(key);
            keys[i] = key;
        }
        return keys;

    }

    public boolean addPlayerToMap(String key, Player p) {
        if (!this.playerMap.containsKey(key)) {
            this.playerMap.put(key, p);
            bank.createAccount(p.getUserID());
            try {
                broker.createAccount(p.getUserID());
            } catch (Exception ex) {

                Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            }
            return true;
        } else {
            System.out.println("Error : key is already associated with a player");
        }
        return false;
    }

    public String getPlayerNameByID(String ID) {
        return this.playerMap.get(ID).getUserName();
    }

    public ArrayList<String[]> getAllPlayerNameList() {
        HashMap<String, Player> map = this.getPlayerMap();
        ArrayList<String[]> outList = new ArrayList<>();

        Iterator<Map.Entry<String, Player>> itr = map.entrySet().iterator();

        while (itr.hasNext()) {
            Map.Entry<String, Player> entry = itr.next();
            String key = entry.getKey();
            String player = entry.getValue().getUserName();
            String[] arr = {key, player};
            outList.add(arr);

        }
        return outList;
    }

    public String getCreatorID() {
        return creatorID;
    }

    public String[] getKeys() {
        return keys;
    }

    public HashMap<String, Player> getPlayerMap() {
        return playerMap;
    }

    public String getGameID() {
        return gameID;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public Status buy(String playerID, String stock, int amount, int stockPrice, int turn) {
        String status_;
        try {
            StockAccount account = broker.buy(playerID, stock, amount, stockPrice, turn, stock);
            String[] event = getEffectiveEventForTurn(currentTurn).split(":");
            String eventString = "NONE", eventTypeString = "NONE";

            if (event.length == 2) {
                eventString = event[0];
                eventTypeString = event[1];
            }
            status_ = "SUCCESSFUL";
            Status status = new Status(status_, bank.getAccount(playerID).getBalance(), account.getStockPortfolio(), eventString, eventTypeString);
            status.setBankTransactions(bank.getAccount(playerID).getTransactions());
            status.setStockTransactions(broker.getStockAccount(playerID).getStockTransactions());

            return status;
        } catch (Exception ex) {
            status_ = ex.getMessage();
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }

        return new Status(status_, 0, null, "", "");
    }

    public Status sell(String playerID, String stock, int amount, int stockPrice, int turn) {
        String status_;
        try {
            StockAccount account = broker.sell(playerID, stock, amount, stockPrice, turn, stock);
            String[] event = getEffectiveEventForTurn(currentTurn).split(":");
            String eventString = "NONE", eventTypeString = "NONE";

            if (event.length == 2) {
                eventString = event[0];
                eventTypeString = event[1];
            }

            status_ = "SUCCESSFUL";
            Status status = new Status(status_, bank.getAccount(playerID).getBalance(), account.getStockPortfolio(), eventString, eventTypeString);
            status.setBankTransactions(bank.getAccount(playerID).getTransactions());
            status.setStockTransactions(broker.getStockAccount(playerID).getStockTransactions());

            return status;
        } catch (Exception ex) {
            status_ = ex.getMessage();
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new Status(status_, 0, null, "", "");
    }

    //Expose Bank methods
    public BankAccount getBankAccount(String playerID) {
        try {
            return bank.getAccount(playerID);
        } catch (Exception ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public StockAccount getStockAccount(String playerID) {
        return broker.getStockAccount(playerID);
    }

    public ArrayList<StockTransaction> getStockStockTransactions(String playerID) {
        return broker.getStockAccount(playerID).getStockTransactions();
    }

    public int getEffectiveStockValue(int turn) {
        if (turn <= cyphers.simulator.Utils.TURNS) {
            int[] event = eventStream.getEventStream();
            return randomTrend[turn] + sectorTrend[turn] + marketTrend[turn] + event[turn];
        }

        return Integer.MAX_VALUE;
    }

    public String getEffectiveEventType() {
        //String looks like EVENT:SUB_EVENT
        return eventStream.getEventInformation();
    }

    public String getEffectiveEventForTurn(int turn) {
        if ((turn - 1) >= eventStream.getEventStartIndex() && (turn - 1) <= eventStream.getEventEndIndex()) {
            return eventStream.getEventInformation();
        }

        return cyphers.simulator.Utils.STRING_EMPTY;
    }

    public List<Company> getCompanyInformation() {
        return companies;
    }

    public boolean setPlayerTurnOver(String playerID) {
        if (!statusIndicator.contains(playerID)) {
            statusIndicator.add(playerID);

            if (statusIndicator.size() == KEYCOUNT) {
                currentTurn = currentTurn + 1;
            }
            return true;
        } else {
            return false;
        }
    }

    public boolean getTurnStatus(int turn) {
        if (turn == currentTurn) {
            return false;
        } else {
            return true;
        }
    }

    public int[] getEffectiveStockValue() {
        int[] effectiveArray = new int[cyphers.simulator.Utils.TURNS];

        for (int i = 0; i < cyphers.simulator.Utils.TURNS; i++) {

            int value = randomTrend[i] + sectorTrend[i] + marketTrend[i] + eventStream.getEventStream()[i];
            if (value < 0) {
                value = 0;
            }

            effectiveArray[i] = value;
        }

        return effectiveArray;
    }

    public Dashboard getDashboardForCurrentTurn() {
        initialDashboard.setCurrentTurn(currentTurn);
        initialDashboard.setGameStatus(getTurnStatus(currentTurn));

        String effectiveEventForTurn = getEffectiveEventForTurn(currentTurn);

        if (!effectiveEventForTurn.isEmpty()) {
            String[] eventInfo = effectiveEventForTurn.split(":");
            initialDashboard.setEvent(eventInfo[0]);
            initialDashboard.setEventType(eventInfo[1]);
        } else {
            initialDashboard.setEvent("No event active at the moment.");
            initialDashboard.setEventType("No event active at the moment.");
        }

        initialDashboard.setEventPrediction(Analyst.getEventRecommendation(eventStream));

        List<AnalystRecommendationConatiner> analystRecommendationList = new ArrayList<>();
        
        for (Company company : companies) {
            AnalystRecommendation recommendation = Analyst.getAnalystRecommendation(getEffectiveStockValue(), company.getPrice());
            AnalystRecommendationConatiner conatiner= new AnalystRecommendationConatiner(company.getStockSymbol(), recommendation);
            analystRecommendationList.add(conatiner);
        }

        initialDashboard.setAnalystRecommendationList(analystRecommendationList);

        int stockValueForTurn = getEffectiveStockValue()[currentTurn - 1];
        List<Integer> comp = new ArrayList<>();
        comp.add(currentTurn);
        for (Company company : companies) {
            double p = (Double.valueOf(stockValueForTurn) / 100d) + cyphers.simulator.Utils.randomInt(1, 2);
            double perc = company.getPrice() * p;
            int netPrice = (int) (company.getPrice() + perc);
            comp.add(netPrice);
            company.setPrice(netPrice);
        }

        List<List<Integer>> chartInfo = initialDashboard.getChartInfo();
        chartInfo.remove(0);
        chartInfo.add(comp);

        initialDashboard.setChartInfo(chartInfo);

        List<PlayerInfo> playerInfo = new ArrayList<>();

        playerMap.values().forEach(player -> {
            try {
                int accountBalance = bank.getAccount(player.getUserID()).getBalance();
                PlayerInfo info = new PlayerInfo(player.getUserID(), player.getUserName(), accountBalance);
                playerInfo.add(info);
            } catch (Exception ex) {
                Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        initialDashboard.setPlayerInfo(playerInfo);

        return initialDashboard;
    }

    public void increaseTurn() {
        currentTurn = currentTurn + 1;
    }

    private Dashboard getDashboardInternal() {
        int[] effectiveStockValue = getEffectiveStockValue();
        int[] unplayables = new int[13];

        System.arraycopy(effectiveStockValue, 0, unplayables, 0, 13);

        List<List<Integer>> chart = new ArrayList<>();

        for (int i = 0; i < unplayables.length; i++) {
            List<Integer> comp = new ArrayList<>();
            comp.add(i + 1);
            for (Company company : companies) {
                double p = (Double.valueOf(unplayables[i]) / 100d) + cyphers.simulator.Utils.randomInt(1, 2);
                double perc = company.getPrice() * p;
                int netPrice = (int) (company.getPrice() + perc);

                comp.add(netPrice);
            }

            chart.add(comp);
        }

        List<PlayerInfo> playerInfo = new ArrayList<>();

        if (playerMap != null) {
            playerMap.values().forEach(player -> {
                try {
                    int accountBalance = bank.getAccount(player.getUserID()).getBalance();
                    PlayerInfo info = new PlayerInfo(player.getUserID(), player.getUserName(), accountBalance);
                    playerInfo.add(info);
                } catch (Exception ex) {
                    Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        }

        Dashboard dashboard = new Dashboard(companies, chart, playerInfo);
        dashboard.setCurrentTurn(currentTurn);
        return dashboard;
    }

    public Dashboard getDashboard() {
        this.initialDashboard = getDashboardInternal();
        return initialDashboard;
    }

    public Status getPlayerStaus(String playerID) {
        Status status;
        Status s = new Status("ERR", 0, null, "", "");
        if (!playerID.isEmpty()) {
            int balance;
            try {
                balance = bank.getAccount(playerID).getBalance();
                HashMap<String, StockEntry> stockPortfolio = broker.getStockAccount(playerID).getStockPortfolio();
                
                status = new Status("P_STATUS", balance, stockPortfolio, "NONE", "NONE");
                
                status.setBankTransactions(bank.getAccount(playerID).getTransactions());
                status.setStockTransactions(broker.getStockAccount(playerID).getStockTransactions());
                return status;
            } catch (Exception ex) {
                s.setStatus(ex.getMessage());
                Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                return s;
            }
        }

        return s;
    }
//</editor-fold>

}
