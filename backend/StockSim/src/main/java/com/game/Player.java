/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.game;

/**
 *
 * @author Shan Wijenayaka
 */
public class Player {

    private final String userName;
    String userID;
    private Game associatedGame;

    public Player(String username, String userID) {
        this.userName = username;
        this.userID = userID;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @return the associatedGame
     */
    public Game getAssociatedGame() {
        return associatedGame;
    }

    /**
     * @param associatedGame the associatedGame to set
     */
    public void setAssociatedGame(Game associatedGame) {
        this.associatedGame = associatedGame;
    }

    /**
     * @return the userID
     */
    public String getUserID() {
        return userID;
    }

    /**
     * @param userID the userID to set
     */
    public void setUserID(String userID) {
        this.userID = userID;
    }
    
}
