package com.game;


    public class PlayerInfo {

        private String playerID;
        private String username;
        private int accountBalance;

        public PlayerInfo(String playerID, String username, int accountBalance) {
            this.playerID = playerID;
            this.username = username;
            this.accountBalance = accountBalance;
        }

        public String getPlayerID() {
            return playerID;
        }

        public void setPlayerID(String playerID) {
            this.playerID = playerID;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public int getAccountBalance() {
            return accountBalance;
        }

        public void setAccountBalance(int accountBalance) {
            this.accountBalance = accountBalance;
        }
    }
