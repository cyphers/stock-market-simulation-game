package com.game;

import cyphers.bank.Transaction;
import cyphers.broker.StockEntry;
import cyphers.broker.StockTransaction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Status {

    private String status;
    private int accountBalance;
    private HashMap<String, StockEntry> stockPortfolio = new HashMap<>();
    private String event;
    private String eventType;
    private ArrayList<StockTransaction> stockTransactions = new ArrayList<>();
    private ArrayList<Transaction> bankTransactions = new ArrayList<>();
    private List<StockPortfolioContainer> stockPortfolioList = new ArrayList<>();

    public Status(String status, int accountBalance, HashMap<String, StockEntry> stockPortfolio, String event, String eventType) {
        this.status = status;
        this.accountBalance = accountBalance;
        this.stockPortfolio = stockPortfolio;
        this.event = event;
        this.eventType = eventType;

        if (stockPortfolio != null) {
            for (Map.Entry<String, StockEntry> entry : stockPortfolio.entrySet()) {
                stockPortfolioList.add(new StockPortfolioContainer(entry.getKey(), entry.getValue()));
            }
        }

    }

    public void setStockPortfolioList(List<StockPortfolioContainer> stockPortfolioList) {
        this.stockPortfolioList = stockPortfolioList;
    }

    public List<StockPortfolioContainer> getStockPortfolioList() {
        return stockPortfolioList;
    }

    public ArrayList<Transaction> getBankTransactions() {
        return bankTransactions;
    }

    public void setBankTransactions(ArrayList<Transaction> bankTransactions) {
        this.bankTransactions = bankTransactions;
    }

    public ArrayList<StockTransaction> getStockTransactions() {
        return stockTransactions;
    }

    public void setStockTransactions(ArrayList<StockTransaction> stockTransactions) {
        this.stockTransactions = stockTransactions;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(int accountBalance) {
        this.accountBalance = accountBalance;
    }

    public HashMap<String, StockEntry> getStockPortfolio() {
        return stockPortfolio;
    }

    public void setStockPortfolio(HashMap<String, StockEntry> stockPortfolio) {
        this.stockPortfolio = stockPortfolio;
    }
}
