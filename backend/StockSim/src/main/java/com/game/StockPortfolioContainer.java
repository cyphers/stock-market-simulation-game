package com.game;

import cyphers.broker.StockEntry;

public class StockPortfolioContainer {

    private String company;
    private StockEntry entry;

    public StockPortfolioContainer(String company, StockEntry entry) {
        this.company = company;
        this.entry = entry;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public StockEntry getEntry() {
        return entry;
    }

    public void setEntry(StockEntry entry) {
        this.entry = entry;
    }
}
