/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils;

import java.util.UUID;

/**
 *
 * @author Shan Wijenayaka
 */
public class Utils {
    public static void main(String[] args) {
        System.out.println(generateKey());
    }

    public static String generateKey() {
        String uuid = UUID.randomUUID().toString().substring(0, 8);
        return uuid;
    }
}
