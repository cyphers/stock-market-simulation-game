package cyphers.analyst;

import cyphers.simulator.Enums;
import cyphers.simulator.EventStream;
import cyphers.simulator.Utils;

public class Analyst {
    public static String getEventRecommendation(EventStream stream) {
        int firstIndex = 0;
        int emptyIndexesBeforeEvent = 0;
        boolean predictionStartPointReached = false;

        // Find the index where the event begins.
        for (int i = 0; i < stream.getEventStream().length; i++) {
            if (stream.getEventStream()[i] != 0) {
                firstIndex = i;
                break;
            }
        }

        // Analyst should detect the event 3 turns before it happens.
        for (int x = 0; x < firstIndex; x++) {
            if (stream.getEventStream()[x] == 0) {
                emptyIndexesBeforeEvent += 1;
            }

            if (emptyIndexesBeforeEvent == 3) {
                predictionStartPointReached = true;
                break;
            }
        }

        if (predictionStartPointReached) {
            return stream.getEventInformation();
        } else {
            return Utils.STRING_EMPTY;
        }
    }
    
    public static AnalystRecommendation getAnalystRecommendation(int[] trendArray, long currentSharePrice) {
        int turnsTillIncrease = 0;
        long sharePrice = currentSharePrice;
        AnalystRecommendation analystRecommendation = new AnalystRecommendation();

        // Calculate percentage increase.
        for (int i = 0; i < trendArray.length; i++) {
            long percentagePrice = (sharePrice * (trendArray[i]) / 100);
            sharePrice = sharePrice + percentagePrice;
            long percentageIncrement = (int) (sharePrice - currentSharePrice);

            if (percentageIncrement == 0) {
                turnsTillIncrease += 1;
            }

            if (percentageIncrement >= 10) {
                analystRecommendation.setRecommendation(Enums.Recommendation.BUY);
            } else {
                analystRecommendation.setRecommendation(Enums.Recommendation.SELL);
            }
        }
        
        analystRecommendation.setTurnsTillRecommendation(turnsTillIncrease);
        return analystRecommendation;
    }
}
