package cyphers.analyst;

import cyphers.simulator.Enums;

public class AnalystRecommendation {

    private Enums.Recommendation recommendation;
    private int turnsTillRecommendation;

    public AnalystRecommendation() {
    }

    public AnalystRecommendation(Enums.Recommendation recommendation, int turnsTillRecommendation) {
        this.recommendation = recommendation;
        this.turnsTillRecommendation = turnsTillRecommendation;
    }

    public Enums.Recommendation getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(Enums.Recommendation recommendation) {
        this.recommendation = recommendation;
    }

    public int getTurnsTillRecommendation() {
        return turnsTillRecommendation;
    }

    public void setTurnsTillRecommendation(int turnsTillRecommendation) {
        this.turnsTillRecommendation = turnsTillRecommendation;
    }
}
