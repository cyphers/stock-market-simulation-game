package cyphers.bank;

import java.util.HashMap;

public class Bank {

    HashMap<String, BankAccount> accounts = new HashMap<>();

    public BankAccount createAccount(String playerID) {
        BankAccount account = new BankAccount();
        accounts.put(playerID, account);
        return account;
    }
    
    public BankAccount getAccount(String playerID) throws Exception {
        BankAccount account = accounts.get(playerID);
        
        if (account != null) {
            return account;
        } else{
            throw new Exception("Account for the given player ID not found.");
        }
    }
}
