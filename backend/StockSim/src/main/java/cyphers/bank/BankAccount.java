package cyphers.bank;

import cyphers.simulator.Enums;
import cyphers.simulator.Utils;
import java.util.ArrayList;

public class BankAccount {

    private int balance;
    private final ArrayList<Transaction> transactions = new ArrayList<>();

    public BankAccount() {
        this.balance = Utils.INITIAL_BALANCE;
    }
    
    public int getBalance() {
        return balance;
    }

    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }

    public int withdraw(int turn, int amount, String involvedPlayerID) throws Exception {
        if (amount > balance) {
            throw new Exception("Not enough balance.");
        } else {
            balance -= amount;
            transactions.add(new Transaction(turn, amount, Enums.TransactionType.WITHDRAWAL, involvedPlayerID));
        }

        return balance;
    }

    public int deposit(int turn, int amount, String involvedPlayerID) {
        balance += amount;
        transactions.add(new Transaction(turn, amount, Enums.TransactionType.DEPOSIT, involvedPlayerID));
        return balance;
    }
}
