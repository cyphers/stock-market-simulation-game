package cyphers.bank;

import cyphers.simulator.Enums.TransactionType;

public class Transaction {

    private final int turn;
    private final int amount;
    private final TransactionType transactionType;
    private final String involvedPlayerID;

    public Transaction(int turn, int amount, TransactionType type, String involvedPlayerID) {
        this.turn = turn;
        this.amount = amount;
        this.transactionType = type;
        this.involvedPlayerID = involvedPlayerID;
    }

    public int getTurn() {
        return turn;
    }

    public int getAmount() {
        return amount;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public String getInvolvedPlayer() {
        return involvedPlayerID;
    }
}
