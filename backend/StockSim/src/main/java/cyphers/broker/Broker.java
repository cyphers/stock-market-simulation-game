package cyphers.broker;

import cyphers.bank.Bank;
import cyphers.bank.BankAccount;
import cyphers.simulator.Enums;
import java.util.HashMap;

public class Broker {

    private final Bank bank;
    private final HashMap<String, StockAccount> stockAccounts = new HashMap<>();

    public Broker(Bank bank) {
        this.bank = bank;
    }

    public StockAccount createAccount(String playerID) throws Exception {
        BankAccount bankAccount = bank.getAccount(playerID);

        if (bankAccount == null) {
            throw new Exception("The given player does not have an associated bank account.");
        } else {
            StockAccount account = new StockAccount();
            stockAccounts.put(playerID, account);
            return account;
        }
    }

    public StockAccount buy(String playerID, String stock, int amount, int stockPrice, int turn, String involvedPlayer) throws Exception {
        BankAccount account = bank.getAccount(playerID);
        if ((amount * stockPrice) > account.getBalance()) {
            throw new Exception("Not enough account balance to buy this amout of stocks.");
        } else {
            int totalAmount = (amount * stockPrice);
            account.withdraw(turn, totalAmount, involvedPlayer);

            StockAccount stockAccount = stockAccounts.get(playerID);
            stockAccount.doTransaction(turn, Enums.StockTransactionType.BUY, stock, amount, stockPrice);

            return stockAccount;
        }
    }

    public StockAccount sell(String playerID, String stock, int amount, int stockPrice, int turn, String involvedPlayer) throws Exception {
        BankAccount account = bank.getAccount(playerID);
        int totalAmount = (amount * stockPrice);
        StockAccount stockAccount = stockAccounts.get(playerID);

        if (stockAccount != null && stockAccount.getStockPortfolio() != null && stockAccount.getStockPortfolio().get(involvedPlayer) != null) {

            if (amount > stockAccount.getStockPortfolio().get(involvedPlayer).getQuantity()) {
                throw new Exception("You don't have enough stocks from this company");
            }

            account.deposit(turn, totalAmount, involvedPlayer);

            stockAccount.doTransaction(turn, Enums.StockTransactionType.SELL, stock, amount, stockPrice);
        }

        return stockAccount;
    }

    public StockAccount getStockAccount(String playerID) {
        return stockAccounts.get(playerID);
    }
}
