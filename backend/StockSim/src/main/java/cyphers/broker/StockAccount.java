package cyphers.broker;

import cyphers.simulator.Enums;
import java.util.ArrayList;
import java.util.HashMap;

public class StockAccount {

    private final ArrayList<StockTransaction> stockTransactions = new ArrayList<>();
    private final HashMap<String, StockEntry> stockPortfolio = new HashMap<>();

    public ArrayList<StockTransaction> getStockTransactions() {
        return stockTransactions;
    }

    public HashMap<String, StockEntry> getStockPortfolio() {
        return stockPortfolio;
    }

    public void doTransaction(int turn, Enums.StockTransactionType type, String stock, int quantity, int price) throws Exception {
        StockTransaction transaction = new StockTransaction(turn, type, stock, quantity, price);
        StockEntry stockEntry = stockPortfolio.get(stock);

        if (stockEntry != null) {
            switch (type) {
                case BUY:
                    int currentQuantity = stockEntry.getQuantity();
                    int totalQuantity = currentQuantity + quantity;
                    stockEntry.setPurchasePrice(price);
                    stockEntry.setQuantity(totalQuantity);
                    stockPortfolio.put(stock, stockEntry);
                    break;
                case SELL:
                    int currentQuantity_ = stockEntry.getQuantity();
                    int totalQuantity_ = currentQuantity_ - quantity;
                    
                    if (totalQuantity_ <= 0) {
                        throw new Exception("Negative quantity");
                    }
                    
                    stockEntry.setPurchasePrice(price);
                    stockEntry.setQuantity(totalQuantity_);
                    stockPortfolio.put(stock, stockEntry);
                    break;
            }
        } else {
            StockEntry entry = new StockEntry();
            entry.setPurchasePrice(price);
            entry.setQuantity(quantity);
            stockPortfolio.put(stock, entry);
        }

        stockTransactions.add(transaction);
    }
}
