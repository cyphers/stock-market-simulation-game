package cyphers.broker;

import cyphers.simulator.Enums.StockTransactionType;


public class StockTransaction {
    private final int turn;
    private final StockTransactionType transactionType;
    private final String stock;
    private final int quantity;
    private final int price;

    public StockTransaction(int turn, StockTransactionType transactionType, String stock, int quantity, int price) {
        this.turn = turn;
        this.transactionType = transactionType;
        this.stock = stock;
        this.quantity = quantity;
        this.price = price;
    }

    public int getTurn() {
        return turn;
    }

    public StockTransactionType getTransactionType() {
        return transactionType;
    }

    public String getStock() {
        return stock;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getPrice() {
        return price;
    }
}
