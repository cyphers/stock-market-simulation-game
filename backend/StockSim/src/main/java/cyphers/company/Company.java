package cyphers.company;


public class Company {
    private final String sector;
    private final String stockSymbol;
    private final String companyName;
    private int price;

    public Company(String sector, String stockSymbol, String companyName, int price) {
        this.sector = sector;
        this.stockSymbol = stockSymbol;
        this.companyName = companyName;
        this.price = price;
    }

    public String getSector() {
        return sector;
    }

    public String getStockSymbol() {
        return stockSymbol;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
}
