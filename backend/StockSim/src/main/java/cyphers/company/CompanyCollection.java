package cyphers.company;

import cyphers.simulator.Utils;
import java.util.ArrayList;
import java.util.List;

public class CompanyCollection {

    public static List<Company> getCompanies() {
        List<Company> companies = new ArrayList<>();
        String[] sectors = new String[]{"FIN", "TEC", "CS", "MAN "};
        String[][] companyNames
                = {
                    {
                        "JPM:JPMorgan Chase & Co.",
                        "BAC:Bank of America Corporation",
                        "WFC:Wells Fargo & Company",
                        "HSBC:HSBC Bank plc.:"
                    },
                    {
                        "AAPL:Apple",
                        "MSFT:Microsoft Corporation",
                        "GOOG:Alphabet Inc.",
                        "FB:Facebook, Inc."
                    },
                    {
                        "AMZN:Amazon.com, Inc.",
                        "WMT:Walmart Inc.",
                        "HD:The Home Depot Inc.",
                        "NFLX:Netflix, Inc."},
                    {
                        "KO:The Coca-Cola Company",
                        "BUD:Anheuser-Busch Inbev SA",
                        "PEP:PepsiCo, Inc.",
                        "PM:Philip Morris International Inc."}
                };

        for (int s = 0; s < sectors.length; s++) {
            for (int c = 0; c < companyNames.length; c++) {
                String[] companyName = (companyNames[s][c]).split(":");
                int stockPrice = Utils.randomInt(Utils.STOCK_PRICE_MIN, Utils.STOCK_PRICE_MAX);
                Company company = new Company(sectors[s], companyName[0], companyName[1], stockPrice);
                companies.add(company);
            }
        }

        return companies;
    }
}
