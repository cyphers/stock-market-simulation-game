package cyphers.simulator;

public class Enums {
    public enum Trend{ 
        INCREASE,
        DECREASE,
        CONSTANT
    }

    public enum Event {
        SECTOR,
        STOCK
    }

    public enum SectorEvent {
        BOOM,
        BUST
    }

    public enum StockEvent {
        PROFITWARNING,
        TAKEOVER,
        SCANDAL
    }
    
    public enum Recommendation {
        BUY,
        SELL
    }
    
    public enum TransactionType {
        DEPOSIT,
        WITHDRAWAL
    }
    
    public enum StockTransactionType {
        BUY,
        SELL
    }
}
