package cyphers.simulator;


public class EventStream {
    private final String eventInformation;
    private final int[] eventStream;
    private final int eventStartIndex;
    private final int eventEndIndex;
    
    public EventStream(String eventInformation, int[] eventStream, int eventStartIndex, int eventEndIndex) {
        this.eventStream = eventStream;
        this.eventInformation = eventInformation;
        this.eventStartIndex = eventStartIndex;
        this.eventEndIndex = eventEndIndex;
    }

    public int[] getEventStream() {
        return eventStream;
    }

    public String getEventInformation() {
        return eventInformation;
    }

    public int getEventStartIndex() {
        return eventStartIndex;
    }

    public int getEventEndIndex() {
        return eventEndIndex;
    }
}
