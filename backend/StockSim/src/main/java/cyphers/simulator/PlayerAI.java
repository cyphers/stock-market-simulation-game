package cyphers.simulator;

import com.game.Game;
import cyphers.analyst.Analyst;
import cyphers.analyst.AnalystRecommendation;
import cyphers.bank.Bank;
import cyphers.broker.Broker;
import cyphers.company.Company;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PlayerAI {

    private String playerAI_ID;
    private final Bank bank;
    private final Broker broker;
    private final Game game;

    public PlayerAI(Bank bank, Broker broker, Game game) {
        this.bank = bank;
        this.broker = broker;
        this.game = game;
        playerAI_ID = game.getGameID() + "__BOT";

        bank.createAccount(playerAI_ID);
        try {
            broker.createAccount(playerAI_ID);
        } catch (Exception ex) {
            Logger.getLogger(PlayerAI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void play(int[] effectiveTrends, List<Company> companies, int currentTurn) {
        Map<String, AnalystRecommendation> recommendations = new HashMap<>();

        for (Company company : companies) {
            AnalystRecommendation recommendation = Analyst.getAnalystRecommendation(effectiveTrends, company.getPrice());
            recommendations.put(company.getStockSymbol(), recommendation);
        }

        Map<String, AnalystRecommendation> buyRecommendations = new HashMap<>();
        Map<String, AnalystRecommendation> sellRecommendations = new HashMap<>();

        for (Map.Entry<String, AnalystRecommendation> entry : recommendations.entrySet()) {
            if (entry.getValue().getRecommendation() == Enums.Recommendation.BUY) {
                buyRecommendations.put(entry.getKey(), entry.getValue());
            } else {
                sellRecommendations.put(entry.getKey(), entry.getValue());
            }
        }

        Enums.Recommendation currentDecision = Enums.Recommendation.values()[Utils.randomInt(0, 1)];

        switch (currentDecision) {
            case BUY:
                if (!buyRecommendations.isEmpty()) {
                    String[] buyingCompanies = buyRecommendations.keySet().toArray(new String[buyRecommendations.size()]);
                    if (buyingCompanies.length != 0) {
                        String comp = buyingCompanies[Utils.randomInt(0, buyingCompanies.length - 1)];
                        int amount = Utils.randomInt(1, 50);
                        Company randomComp = companies.stream().filter(c -> c.getStockSymbol().equals(comp)).findFirst().orElse(null);

                        if (randomComp != null) {
                            try {
                                broker.buy(playerAI_ID, randomComp.getStockSymbol(), amount, randomComp.getPrice(), currentTurn, comp);
                            } catch (Exception ex) {
                                Logger.getLogger(PlayerAI.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                }
                break;
            case SELL:
                if (!sellRecommendations.isEmpty()) {
                    String[] sellingCompanies = sellRecommendations.keySet().toArray(new String[buyRecommendations.size()]);
                    if (sellingCompanies.length != 0) {
                        String comp = sellingCompanies[Utils.randomInt(0, sellingCompanies.length - 1)];
                        int amount = Utils.randomInt(1, 50);
                        Company randomComp = companies.stream().filter(c -> c.getStockSymbol().equals(comp)).findFirst().orElse(null);

                        if (randomComp != null) {
                            try {
                                broker.sell(playerAI_ID, randomComp.getStockSymbol(), amount, randomComp.getPrice(), currentTurn, comp);
                            } catch (Exception ex) {
                                Logger.getLogger(PlayerAI.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                }
                break;
        }

    }
}
