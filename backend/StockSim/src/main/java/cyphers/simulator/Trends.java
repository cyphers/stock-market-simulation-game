package cyphers.simulator;

import cyphers.simulator.Enums.Trend;
import java.util.ArrayList;
import java.util.Random;

public class Trends {

    private static int[] getPartitions(int partitionCount, int turns) {
        Random random = new Random();
        int numbers[] = new int[partitionCount];
        int sum = 0;

        for (int i = 0; i < partitionCount - 1; i++) {
            numbers[i] = random.nextInt((turns - sum) / 3) + 3;
            sum += numbers[i];
        }

        numbers[partitionCount - 1] = turns - sum;

        return numbers;
    }

    public static int[] getRandomTrend() {
        Trend[] trends = new Trend[Utils.PARTITION_COUNT];
        int[] partitions = getPartitions(Utils.PARTITION_COUNT, Utils.TURNS);
        ArrayList<Integer> randomTrend = new ArrayList<>();

        // Randomly generate what trend should be applied to each partition.
        for (int i = 0; i < 3; i++) {
            trends[i] = Enums.Trend.values()[new Random().nextInt(Utils.PARTITION_COUNT)];
        }

        // Go through each partition and fill that partition with relevant trend value.
        for (int i = 0; i < partitions.length; i++) {
            switch (trends[i]) {
                case CONSTANT:
                    for (int x = 0; x < partitions[i]; x++) {
                        randomTrend.add(0);
                    }
                    break;
                case INCREASE:
                    for (int x = 0; x < partitions[i]; x++) {
                        randomTrend.add(Utils.randomInt(0, 2));
                    }
                    break;
                case DECREASE:
                    for (int x = 0; x < partitions[i]; x++) {
                        randomTrend.add(Utils.randomInt(-2, 0));
                    }
                    break;
            }
        }

        return randomTrend.stream().mapToInt(index -> index).toArray();
    }

    public static EventStream getEventStream() {
        int[] eventStream = new int[Utils.TURNS];
        long probability = 0;

        int eventStartIndex = Utils.randomInt(1, Utils.TURNS - 1);
        System.out.println("EVENT_START: " + eventStartIndex);

        long probabilityIncrement = 100 / Utils.TURNS;
        Enums.Event eventType;
        String subEvent = Utils.STRING_EMPTY;

        // Initialize eventStream values to 0.
        for (int i = 0; i < eventStream.length; i++) {
            eventStream[i] = 0;
        }

        for (int i = 0; i < eventStream.length; i++) {
            // Prevent event happening in the first turn.
            if (i == 0) {
                probability += probabilityIncrement;
                continue;
            }

            // Increase probability of an event happening.
            if (i == eventStartIndex) {
                break;
            } else {
                probability += probabilityIncrement;
            }
        }

        // Decide what sort of event should happen based on the probability.
        if (probability <= 33) {
            eventType = Enums.Event.SECTOR;
        } else {
            eventType = Enums.Event.STOCK;
        }

        // Generate random event length.
        int eventLength = Utils.randomInt(1, Utils.TURNS - eventStartIndex);
        System.out.println("EVENT_LENGTH: " + eventLength);

        int loopIndex = (eventLength + eventStartIndex);
        System.out.println("LOOP_INDEX: " + loopIndex);

        switch (eventType) {
            case SECTOR:
                Enums.SectorEvent sectorEvent = Enums.SectorEvent.values()[new Random().nextInt(2)];
                subEvent = sectorEvent.toString();

                switch (sectorEvent) {
                    case BOOM:
                        int boomValue = Utils.randomInt(1, 5);

                        for (int x = eventStartIndex; x < loopIndex; x++) {
                            eventStream[x] = boomValue;
                        }
                        break;
                    case BUST:
                        int bustValue = Utils.randomInt(-5, -1);

                        for (int x = eventStartIndex; x < loopIndex; x++) {
                            eventStream[x] = bustValue;
                        }
                        break;
                }
                break;
            case STOCK:
                Enums.StockEvent stockEvent = Enums.StockEvent.values()[new Random().nextInt(3)];
                subEvent = stockEvent.toString();

                switch (stockEvent) {
                    case PROFITWARNING:
                        int profitValue = Utils.randomInt(2, 3);

                        for (int x = eventStartIndex; x < loopIndex; x++) {
                            eventStream[x] = profitValue;
                        }
                        break;
                    case SCANDAL:
                        int scandalValue = Utils.randomInt(-6, -3);

                        for (int x = eventStartIndex; x < loopIndex; x++) {
                            eventStream[x] = scandalValue;
                        }
                        break;
                    case TAKEOVER:
                        int takeoverValue = Utils.randomInt(-5, -1);

                        for (int x = eventStartIndex; x < loopIndex; x++) {
                            eventStream[x] = takeoverValue;
                        }
                        break;
                }

                break;
        }

        return new EventStream(eventType.toString() + ":" + subEvent, eventStream, eventStartIndex, eventLength - 1);
    }

    public static int[] getTrend() {
        int[] trends = new int[Utils.TURNS];
        for (int i = 0; i < Utils.TURNS; i++) {
            if (i == 0) {
                trends[i] = Utils.randomInt(-3, 3);
                continue;
            }

            int difference = 0;
            int value = 0;

            while (difference != 1) {
                value = Utils.randomInt(-3, 3);
                difference = Math.abs(trends[i - 1] - value);
            }

            trends[i] = value;
        }
        
        return trends;
    }
}
