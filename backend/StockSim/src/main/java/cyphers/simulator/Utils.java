package cyphers.simulator;

import java.util.Random;

public class Utils {

    public static final int TURNS = 20;
    public static final int PARTITION_COUNT = 3;
    public static final String STRING_EMPTY = "";
    public static final int INITIAL_BALANCE = 10000000;
    public static final int STOCK_PRICE_MIN = 50;
    public static final int STOCK_PRICE_MAX = 150;
    
    public static int randomInt(int min, int max) {
        Random random = new Random();
        return random.nextInt(max + 1 - min) + min;
    }

    public static String getRandomID() {
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder builder = new StringBuilder();
        Random random = new Random();

        while (builder.length() < 8) {
            int index = (int) (random.nextFloat() * chars.length());
            builder.append(chars.charAt(index));
        }

        return builder.toString();
    }
}
