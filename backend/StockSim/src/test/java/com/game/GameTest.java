/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.game;

import com.google.gson.Gson;
import cyphers.bank.BankAccount;
import org.junit.Test;

/**
 *
 * @author lakglk
 */
public class GameTest {

    public GameTest() {
    }

    @Test
    public void testSomeMethod() {
//        Player p = new Player("", "");
//        Player p1 = new Player("", "");
//        p1.setUserID("0000001");
//        Player p2 = new Player("", "");
//        p2.setUserID("0002");
//        Player p3 = new Player("", "");
//        p3.setUserID("0003");
//        String[] s = new String[]{"", "", "", ""};
//        Game game = new Game("", p, s);
//        game.addPlayerToMap("1", p1);
//        game.addPlayerToMap("2", p2);
//        game.addPlayerToMap("3", p3);
//        Gson g = new Gson();
//
//        game.buy("003", "mSFT", 5, 10, 0);
//        game.buy("0002", "mSFT", 5, 10, 0);
//
//        Dashboard dashboard = game.getDashboardForCurrentTurn();
//        List<List<Integer>> chartInfo = dashboard.getChartInfo();
//
//        List<Company> companies = dashboard.getCompanies();
//        List<String> collect = companies.stream().map(e -> e.getStockSymbol()).collect(Collectors.toList());
//        String[] stringNames = collect.stream().toArray(String[]::new);
//
//        String compNamesJson = g.toJson(stringNames);
//        String j1 = g.toJson(chartInfo);
//
//        game.increaseTurn();
//
//        Dashboard d1 = game.getDashboardForCurrentTurn();
//
//        String j2 = g.toJson(d1.getChartInfo());
//
//        System.err.println("");

        Player player02 = new Player("PLAYER_02", "p_02");
        Player player03 = new Player("PLAYER_03", "p_03");

        Game game = new Game("p_01", new Player("PLAYER_01  ", "p_01"), new String[]{"p_01"});
        game.addPlayerToMap("p_02", player02);
        game.addPlayerToMap("p_03", player03);

        Gson gson = new Gson();
        game.getDashboard();
        String jsonDashboardInitial = gson.toJson(game.getDashboard());
        
        
        Status buy = game.buy("p_01", "MSFT", 5, 10, 1);
        BankAccount bankAccount = game.getBankAccount("p_01");

        Dashboard dashboardForCurrentTurn01 = game.getDashboardForCurrentTurn();
        game.setPlayerTurnOver("p_02");
        game.setPlayerTurnOver("p_03");
        game.setPlayerTurnOver("p_01");

        
        game.sell("p_01", "MSFT", 2, 5, 3);
        
        Dashboard dashboardForCurrentTurn02 = game.getDashboardForCurrentTurn();

        game.setPlayerTurnOver("p_02");
        game.setPlayerTurnOver("p_03");
        game.setPlayerTurnOver("p_01");

        Dashboard dashboardForCurrentTurn03 = game.getDashboardForCurrentTurn();

    }
}
