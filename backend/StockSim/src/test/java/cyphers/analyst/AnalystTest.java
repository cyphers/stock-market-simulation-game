package cyphers.analyst;

import cyphers.simulator.Enums;
import cyphers.simulator.EventStream;
import cyphers.simulator.Utils;
import static org.junit.Assert.*;
import org.junit.Test;

public class AnalystTest {

    public AnalystTest() {
    }

    @Test
    public void testGetEventRecommendation() {
        EventStream stream01 = new EventStream("TEST:TEST", new int[]{0, 0, 0, 1, 1, 1, 1, 1, 1, 1}, 0, 0);
        String event01 = Analyst.getEventRecommendation(stream01);
        assertEquals("Event should be predicted 3 turns before it happens.", true, !event01.equals(Utils.STRING_EMPTY));

        EventStream stream02 = new EventStream("TEST:TEST", new int[]{0, 1, 1, 1, 1, 1, 1, 1, 1, 1}, 0, 0);
        String event02 = Analyst.getEventRecommendation(stream02);
        assertEquals("Event should be predicted 3 turns before it happens.", false, !event02.equals(Utils.STRING_EMPTY));
    }

    @Test
    public void testGetAnalystRecommendation() {
        int[] values01 = new int[]{0, 0, 0, 0, 0, 8, 0, 0, 0, 0};
        AnalystRecommendation recommendation01 = Analyst.getAnalystRecommendation(values01, 100);
        assertEquals("If share price will fall by more than 10%, the analyst issues a SELL.",
                Enums.Recommendation.SELL, recommendation01.getRecommendation());

        int[] values02 = new int[]{0, 0, 0, 0, 0, 8, 8, 8, 0, 0};
        AnalystRecommendation recommendation02 = Analyst.getAnalystRecommendation(values02, 100);
        assertEquals("If share price will increase by more than 10%, the analyst issues a BUY.",
                Enums.Recommendation.BUY, recommendation02.getRecommendation());
    }
}
