package cyphers.bank;

import java.util.ArrayList;
import java.util.UUID;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

public class AccountTest {

    private final Bank bank = new Bank();
    BankAccount account = bank.createAccount(UUID.randomUUID().toString());
    String randomPlayerID = UUID.randomUUID().toString();

    public AccountTest() {
    }

    @Test
    public void testGetBalance() {
        account.deposit(0, 1500, randomPlayerID);
        int balance = account.getBalance();
//        assertEquals("getBalance() should return the account balance after deposit/withdraw operation", 2500, balance);
    }

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void testWithdraw() throws Exception {
        account.deposit(0, 1000, UUID.randomUUID().toString());
        int balance = account.withdraw(999, 500, randomPlayerID);
      //  assertEquals("Method should return the balance after withdrawing.", 1500, balance);

        ArrayList<Transaction> transactions = account.getTransactions();
        // Test on the last Transaction object. [999, 500, randomPlayerID]
        Transaction transaction = transactions.get(transactions.size() - 1);

        assertEquals("Transaction amount should be equal to the value passed.", 500, transaction.getAmount());
        assertEquals("Turn should be equal to the value passed.", 999, transaction.getTurn());
        assertEquals("Invovled player ID should be equal to the value passed.", randomPlayerID, transaction.getInvolvedPlayer());

       // exception.expect(Exception.class);
        account.withdraw(0, 5000, randomPlayerID);
    }

    @Test
    public void testDeposit() {
        int balance = account.deposit(0, 5000, randomPlayerID);
//        assertEquals("Method should return the balance after adding the passed amount.", 6000, balance);
    }

    @Test
    public void testGetTransactions() {
        boolean isCorrectMainType = account.getTransactions() instanceof ArrayList<?>;
        assertEquals("Transactions shoul be an ArrayList.", true, isCorrectMainType);
    }

}
