package cyphers.bank;

import java.util.UUID;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

public class BankTest {

    private final Bank bank;

    public BankTest() {
        this.bank = new Bank();
    }

    @Test
    public void testCreateAccount() throws Exception {
        String playerID = UUID.randomUUID().toString();
        BankAccount account = bank.createAccount(playerID);

        assertEquals("Bank should return the same account which it created.", account, bank.getAccount(playerID));
    }

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void testGetAccount() throws Exception {
        String playerID01 = UUID.randomUUID().toString();
        String playerID02 = UUID.randomUUID().toString();

        BankAccount account01 = bank.createAccount(playerID01);
        BankAccount account02 = bank.createAccount(playerID02);

        assertNotEquals("Method should return correct account when the Player ID is passed.", account02, bank.getAccount(playerID01));
        assertEquals("Method should return correct account when the Player ID is passed.", account01, bank.getAccount(playerID01));
        
        exception.expect(Exception.class);
        bank.getAccount(UUID.randomUUID().toString());
    }

}
