package cyphers.broker;

import cyphers.bank.Bank;
import java.util.UUID;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class BrokerTest {

    private final Bank bank = new Bank();
    private final String playerID = UUID.randomUUID().toString();
    String playerID01 = UUID.randomUUID().toString();
    String playerID02 = UUID.randomUUID().toString();
    Broker broker = new Broker(bank);

    public BrokerTest() {
        bank.createAccount(playerID);
    }

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void testCreateAccount() throws Exception {
        StockAccount account = broker.createAccount(playerID);
        assertEquals("A bank account should be created before a stock account is created.", false, (account == null));

        exception.expect(Exception.class);
        broker.createAccount(UUID.randomUUID().toString());
    }

    @Test
    public void testBuy() throws Exception {

        bank.createAccount(playerID01);
        broker.createAccount(playerID01);
        StockAccount account = broker.buy(playerID01, "TEST", 10, 50, 0, UUID.randomUUID().toString());

        StockEntry entry = account.getStockPortfolio().get("TEST");
        assertEquals("Stock entry should contain the stock and quantity.", 10, entry.getQuantity());
    }

    @Test
    public void testSell() throws Exception {
        bank.createAccount(playerID02);
        broker.createAccount(playerID02);
        broker.buy(playerID02, "TEST", 10, 50, 0, UUID.randomUUID().toString());
        StockAccount account02 = broker.sell(playerID02, "TEST", 5, 50, 0, UUID.randomUUID().toString());

        StockEntry entry = account02.getStockPortfolio().get("TEST");
        assertEquals("Stock entry should contain the stock and quantity.", 10, entry.getQuantity());
    }

}
