package cyphers.broker;

import cyphers.simulator.Enums;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.*;
import org.junit.Test;

public class StockAccountTest {
    private final StockAccount stockAccount = new StockAccount();
    public StockAccountTest() {
    }

    @Test
    public void testDoTransaction() {
        int quantity = 100;
        int purchasePrice = 100;
        Enums.StockTransactionType type = Enums.StockTransactionType.BUY;
        String stock = "STOCK";
        
        try {
            stockAccount.doTransaction(0, type, stock, quantity, purchasePrice);
        } catch (Exception ex) {
            Logger.getLogger(StockAccountTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        HashMap<String, StockEntry> stockPortfolio = stockAccount.getStockPortfolio();
        ArrayList<StockTransaction> stockTransactions = stockAccount.getStockTransactions();
        
        StockTransaction transaction = stockTransactions.get(stockTransactions.size() - 1);
        
        assertEquals("Stock transaction should contain correct quantity.", quantity, transaction.getQuantity());
        assertEquals("Stock transaction should contain correct price.", purchasePrice, transaction.getPrice());
        assertEquals("Stock transaction should contain correct transaction type.", Enums.StockTransactionType.BUY, transaction.getTransactionType());
        assertEquals("Stock transaction should contain correct stock name.", stock, transaction.getStock());
        assertEquals("Stock portfolio should return correct StockEntry object.", quantity , stockPortfolio.get(stock).getQuantity());
        
    }
    
}
