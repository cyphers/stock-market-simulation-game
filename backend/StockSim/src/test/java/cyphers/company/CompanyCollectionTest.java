package cyphers.company;

import cyphers.simulator.Utils;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;

public class CompanyCollectionTest {

    @Test
    public void testGetCompanies() {
        List<Company> companies = CompanyCollection.getCompanies();

        companies.forEach((company) -> {
            assertEquals("Share price should be between STOCK_PRICE_MIN and STOCK_PRICE_MAX",
                    true,
                    (company.getPrice() >= Utils.STOCK_PRICE_MIN && company.getPrice() <= Utils.STOCK_PRICE_MAX));
        });
    }

}
