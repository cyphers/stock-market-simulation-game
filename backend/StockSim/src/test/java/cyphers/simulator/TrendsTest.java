package cyphers.simulator;

import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class TrendsTest {

    public TrendsTest() {
    }

    @Test
    public void testGetRandomTrend() {
        List<Integer> possibleValueList = Arrays.asList(-2, -1, 0, 1, 2);
        int[] randomTrend = Trends.getRandomTrend();
        System.err.println(Arrays.toString(randomTrend));
        boolean result = Arrays.asList()
                .stream().allMatch(value -> Arrays.asList(possibleValueList).contains(value));
        assertEquals("All values are in the expected range of [-2, -1, 0, 1, 2].", true, result);
    }

    @Test
    public void testGetEventStream() {
        EventStream stream = Trends.getEventStream();

        boolean isEventOnFirstTurn = stream.getEventStream()[0] == 0;
        assertEquals("Probability of an event occurring on first turn should be 0.", true, isEventOnFirstTurn);

        String[] split = stream.getEventInformation().split(":");

        switch (split[0]) {
            case "SECTOR":
                switch (split[1]) {
                    case "BOOM":
                        List<Integer> values01 = Arrays.asList(1, 2, 3, 4, 5);
                        boolean result01 = Arrays.asList()
                                .stream().allMatch(value -> Arrays.asList(values01).contains(value));
                        assertEquals("All values are in the expected range of [1, 2, 3, 4, 5].", true, result01);
                        break;
                    case "BUST":
                        List<Integer> values02 = Arrays.asList(-5, -4, -3, -2, -1);
                        boolean result02 = Arrays.asList()
                                .stream().allMatch(value -> Arrays.asList(values02).contains(value));
                        assertEquals("All values are in the expected range of [-5, -4, -3, -2, -1].", true, result02);
                        break;
                }
                break;
            case "STOCK":
                switch (split[1]) {
                    case "PROFITWARNING":
                        List<Integer> values03 = Arrays.asList(2, 3);
                        boolean result03 = Arrays.asList()
                                .stream().allMatch(value -> Arrays.asList(values03).contains(value));
                        assertEquals("All values are in the expected range of [2, 3].", true, result03);
                        break;
                    case "TAKEOVER":
                        List<Integer> values04 = Arrays.asList(-5, -4, -3, -2, -1);
                        boolean result04 = Arrays.asList()
                                .stream().allMatch(value -> Arrays.asList(values04).contains(value));
                        assertEquals("All values are in the expected range of [-5, -4, -3, -2, -1].", true, result04);
                        break;
                    case "SCANDAL":
                        List<Integer> value05 = Arrays.asList(-6, -5, -4, -3, -2, -1);
                        boolean result05 = Arrays.asList()
                                .stream().allMatch(value -> Arrays.asList(value05).contains(value));
                        assertEquals("All values are in the expected range of [-6, -5, -4, -3, -2, -1].", true, result05);
                        break;

                }
                break;
        }
    }

    @Test
    public void testGetTrend() {
        int[] trends = Trends.getTrend();
        for (int i = 1; i < trends.length; i++) {
            int difference = Math.abs((trends[i] - 1) - trends[i]);
            
            if (difference != 0) {
                assertEquals("Consecutive values can differ by at most 1 between turns.", 1, difference);
            }
        }
    }
}
