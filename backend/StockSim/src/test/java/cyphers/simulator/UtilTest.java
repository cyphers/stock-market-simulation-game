package cyphers.simulator;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class UtilTest {
    
    public UtilTest() {
    }

    @Test
    public void testRandomInt() {
        int min = -20;
        int max = +20;
        List<Integer> values = new ArrayList<>();
        
        for (int i = 0; i < 20; i++) {
            values.add(Utils.randomInt(min, max));
        }
        
        boolean isInRange = values.stream().allMatch(value -> (value >= min && value <= max));
        assertEquals(true, isInRange);
    }
    
}
