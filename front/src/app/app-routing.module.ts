import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
const routes: Routes = [
  {
    path : '',
    component : AppComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path : 'dashboard',
    component : DashboardComponent
  }
];

@NgModule({
  imports: [    RouterModule.forRoot(
    routes // <--   ,{ enableTracing: true }  debugging purposes only shan
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
