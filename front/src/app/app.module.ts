import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { SearchPipe } from './shared/pipes/search.pipe';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './components/login/login.component';
import { PlayersComponent } from './components/players/players.component';
import { PlayerComponent } from './components/player/player.component';
import { StockComponent } from './components/stock/stock.component';
import { HistoryComponent } from './components/history/history.component';
import { HomeComponent } from './components/home/home.component';
import { EventComponent } from './components/event/event.component';
 import { TimerComponent } from './components/timer/timer.component';


@NgModule({
  declarations: [
    SearchPipe,
    AppComponent,
    DashboardComponent,
    LoginComponent,
    PlayersComponent,
    PlayerComponent,
    StockComponent,
    HistoryComponent,
    EventComponent,
    HomeComponent,
     TimerComponent
  ],
  exports: [
    SearchPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
