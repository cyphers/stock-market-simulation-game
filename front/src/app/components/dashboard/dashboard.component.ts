import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DataService } from '../../data.service';
import { Observable, Subscription, observable } from 'rxjs';
declare var google: any;

import { IPlayer } from '../../shared/core/player';
import { Stock } from '../../shared/core/stock';
import { Event } from '../../shared/core/event';
import { History } from '../../shared/core/history';
import { Dashboard } from '../../shared/core/dashboard';
import { PlayerInfo } from '../../shared/core/playerInfo';
import { PlayerStock } from '../../shared/core/playerStock';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {


  TURN_COUNT = 20;
  TURN_DURATION = 10;
  curTurn = 0;
  clockVal: number;
  timer: any;
  users$: object;
  userId: string;
  currentTurn = 1;
  totalTurns = 20;
  selectedPlayer: IPlayer; // selected player to View History
  loggedplayer: IPlayer; // current/logged player
  snackMsg = "test";

  //#region Player
  selectedStock: Stock;  // selected stock for buying
  selectedPlayerStock: PlayerStock; // selected stock for sell
  totalPrice: number;  // total price of selling/ buying
  sellingQuantity: number;
  isError: boolean;  // error msg when buying/selling
  playerStock: Stock[] = []; // stocks of the logged player
  //#endregion Player

  //#region  Players
  players: IPlayer[] = [];
  //#endregion Players

  //#region Stock
  stocks: Stock[] = [];
  txtSearch: string; // keyword to search the stock table
  //#endregion Stock

  //#region Timer
  ticks = 0;
  minutesDisplay = 0;
  hoursDisplay = 0;
  secondsDisplay = 0;
  sub: Subscription;
  //#endregion Timer

  events: Event[] = [];
  history: History[] = [];
  analystDecision: any;

  player$;
  playerName: string;
  playerID: string;
  dashboard: Dashboard;
  playerInfo: PlayerInfo;
  eventPrediction:string="No event active at the moment.";

  constructor(private data: DataService) {

  }

  ngOnInit() {


    this.player$ = JSON.parse(localStorage.getItem('cipher_player'));
    this.playerName = this.player$['userName'];
    this.playerID = this.player$['userID'];
    this.clockVal = 0;
    this.curTurn = 0;
    this.clockVal = 0;
    
    setTimeout(() => {
      console.log('running');
      this.turnLoader();
    }, 1000);


    // initialize selected stock
    this.selectedStock = new Stock();
    this.selectedPlayer = new IPlayer();
    this.selectedPlayerStock = new PlayerStock();

    //this.userId = window.localStorage.getItem('userId');

    this.getLoggedPlayer();
    this.getPlayers();
    this.getEvents();
    //this.getStocks();

    this.startTimer();
     //get game data from backend
     this.getDashboard();
  }

  turnLoader() {

   if (this.curTurn === 0) {
    this.curTurn++;
    this.getDashboard();
   }

    const turnInterval = setInterval(() => {
      console.log('xxxxxxxxx');
      this.curTurn++;
      if (this.curTurn >= this.TURN_COUNT) {
        console.log('game over');
        this.setClock(false);
        clearInterval(turnInterval);
      } else {

        if(!this.dashboard){
          this.getDashboard();
        } else {
          this.getNextTurn();
        }
      }

    }, this.TURN_DURATION * 1000);

  }

  getDashboard() {
   
    this.data.apiCall('initializeDashboard', '', this.playerID ).subscribe(
     data => { 
       console.log(data['res'] as Dashboard); 
       this.setClock(true);
       this.dashboard = data['res'] as Dashboard;
       this.stocks = this.dashboard.companies;
       this.analystDecision = this.dashboard.analystRecommendations;
       // this.events = this.dashboard.eventPrediction;
       this.players = this.dashboard.playerInfo;
       this.loggedplayer = this.players.find(player=>player.playerID == this.playerID);

       this.loadChart(data['res']);
     });
 }

  getNextTurn() {
//     event: "No event active at the moment."
// ​
// eventPrediction: "STOCK:TAKEOVER"
// ​
// eventType: "No event active at the moment."

    this.data.apiCall('setPlayerTurnOver', 'no data', this.playerID).subscribe(
      d => {
        const getTurnStatusInterval = setInterval(()=>{
          this.data.apiCall('getTurnStatus', this.curTurn.toString() , this.playerID).subscribe(
            dataX => {
              const res = dataX['res'];
              console.log(res);
              if(res){
                clearInterval(getTurnStatusInterval);
                this.data.apiCall('getDashboardForCurrentTurn', '', this.playerID ).subscribe(
                  data => {
                    console.log(data['res'] as Dashboard); 
                    this.setClock(true);
                    this.dashboard = data['res'] as Dashboard;
                    this.stocks = this.dashboard.companies;
                    this.analystDecision = this.dashboard.analystRecommendations;
                    // this.events = this.dashboard.eventPrediction;
                    this.players = this.dashboard.playerInfo;
                    this.loadChart(data['res']);
                  });
              }
            });
        }, 2000);

        });
  }

  setClock(val) {
    if (val) {
      if (this.timer) {
        clearInterval(this.timer);
      }
      this.clockVal = this.TURN_DURATION;
      this.timer = setInterval(() => {
        this.clockVal -= 1;
        console.log('remains: ', this.clockVal);
      }, 1000);
    } else {
      clearInterval(this.timer);
      this.clockVal = 0;
    }

  }

  loadChart(resData) {

    google.charts.load('current', { 'packages': ['line'] });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

      const data = new google.visualization.DataTable();

      data.addColumn('number', 'Turn');
      resData.companies.forEach(company => {
        data.addColumn('number', company.companyName);
      });

      data.addRows(resData.chartInfo);
 // ((window.innerWidth) / 12 * 10 ) - 50,
      const options = {
        backgroundColor: '#e6e6e6' ,
        'height':600,
        'width':  '100%', 
        aggregationTarget: 'none',
        animation: {
          duration: 2000,
          startup: true
        },
        explorer: true,
        // colors:['red','#004411'].
        // chart: {
        //   title: 'Stock Prices',
        //   subtitle: 'in Rupees (Rs)'
        // },
        hAxis: {
          allowContainerBoundaryTextCufoff: true,
          showTextEvery: 1,
          format: 'short'
        },
        axes: {
          // x: {
          //   0: { side: 'top' }
          // }
        }
      };

      const chart = new google.charts.Line(document.getElementById('stockChart'));

      chart.draw(data, google.charts.Line.convertOptions(options));
    }
  }

  //#region  Player

  getLoggedPlayer() {
    const player = new IPlayer();
    player.username = 'Alice';
    player.rank = 3;
    player.avatar = '../../../assets/images/female.jpg';
    player.accountBalance = 500.00;
    player.totalStock = 100;
    player.profit = 250;
    player.stocks = [];
    //player.stocks = this.getPlayerStock(name);
    this.loggedplayer = player;
  }

  getPlayerStock(name: string) {

    const stock1 = new Stock();
    stock1.sector = 'IT';
    stock1.companyName = 'Apple';
    stock1.price = 200;
    stock1.quantity = 50;
    this.playerStock.push(stock1);

    const stock2 = new Stock();
    stock2.sector = 'IT';
    stock2.companyName = 'Google';
    stock2.price = 150;
    stock2.quantity = 150;
    this.playerStock.push(stock2);
    return this.playerStock;
  }

  sellStock(stock: PlayerStock) {
    this.selectedPlayerStock = stock;
    this.selectedPlayerStock.sector = this.stocks.find(i=>i.stockSymbol==this.selectedPlayerStock.company).sector;
    this.sellingQuantity = null;
    this.totalPrice = 0;
    this.isError = false;
  }

  onSetQuantity(event: any) {
    const value = event.target.value;
    this.sellingQuantity = value;
    this.totalPrice = this.selectedPlayerStock.purchasePrice * value;

    this.isError = (this.sellingQuantity > this.selectedPlayerStock.quantity) ? true : false;
  }

  submitSellStock() {
      // tslint:disable-next-line:max-line-length
      const dataArr = [this.playerID, this.selectedPlayerStock.company, this.selectedPlayerStock.quantity.toString(), this.selectedPlayerStock.purchasePrice.toString(), this.curTurn.toString()];
      this.data.apiCall('sell', dataArr , JSON.parse(localStorage.getItem('cipher_player'))['userID']).subscribe(
        data => {
  
          console.log(data);
          const res = data['res'];
          this.showSnackBar(res.status);
          this.loggedplayer.accountBalance -= this.totalPrice;
        });
  }

  showHistory(player: IPlayer, index: number) {
    this.selectedPlayer = player;
    this.selectedPlayer.rank = index;
    this.getHistory(this.selectedPlayer.playerID);
  }
  //#endregion Player

  //#region  Players
  getPlayers() {
    const player1 = new IPlayer();
    player1.username = 'Tom';
    player1.rank = 1;
    player1.avatar = '../../../assets/images/male.png';
    this.players.push(player1);

    const player2 = new IPlayer();
    player2.username = 'AI player';
    player2.rank = 2;
    player2.avatar = '../../../assets/images/AI.png';
    this.players.push(player2);

    const player3 = new IPlayer();
    player3.username = 'Alice';
    player3.rank = 3;
    player3.avatar = '../../../assets/images/female.jpg';
    player3.isLogged = true;
    this.players.push(player3);

    const player4 = new IPlayer();
    player4.username = 'Bob';
    player4.rank = 4;
    player4.avatar = '../../../assets/images/male.png';
    this.players.push(player4);
  }
  //#endregion Players

  //#region  Events
  getEvents() {
    // get backend data
    const event1 = new Event();
    event1.turnNo = 2;
    event1.evenType = 'Sector';
    event1.eventName = 'BOOM';

    const event2 = new Event();
    event2.turnNo = 3;
    event2.evenType = 'Stock';
    event2.eventName = 'PROFIT_WARNING';

    const event3 = new Event();
    event3.turnNo = 4;
    event3.evenType = 'Sector';
    event3.eventName = 'BUST';

    this.events.push(event1);
    this.events.push(event2);
    this.events.push(event3);
  }
  //#endregion Events

  //#endregion Stock
  getStocks() {
    this.stocks = [];

    const stock1 = new Stock();
    stock1.sector = 'IT';
    stock1.companyName = 'Apple';
    stock1.price = 200;
    this.stocks.push(stock1);

    const stock2 = new Stock();
    stock2.sector = 'IT';
    stock2.companyName = 'Google';
    stock2.price = 150;
    this.stocks.push(stock2);

    const stock3 = new Stock();
    stock3.sector = 'Finance';
    stock3.companyName = 'ABC';
    stock3.price = 250;
    this.stocks.push(stock3);

    const stock4 = new Stock();
    stock4.sector = 'IT';
    stock4.companyName = 'Apple';
    stock4.price = 200;
    this.stocks.push(stock4);

    const stock5 = new Stock();
    stock5.sector = 'IT';
    stock5.companyName = 'Google';
    stock5.price = 150;
    this.stocks.push(stock5);

    const stock6 = new Stock();
    stock6.sector = 'Finance';
    stock6.companyName = 'ABC';
    stock6.price = 250;
    this.stocks.push(stock6);
  }

  buyStock(stock: Stock) {

    this.selectedStock = stock;
    this.selectedStock.quantity = null;
    this.totalPrice = 0;
  }

  onSetBuyingQuantity(event: any) {
    const value = event.target.value;
    this.selectedStock.quantity = value;
    this.totalPrice = this.selectedStock.price * value;

    this.isError = (this.loggedplayer.accountBalance < this.totalPrice) ? true : false;
  }

  submitBuyStock() {
    const dataArr = [this.playerID, this.selectedStock.stockSymbol, this.selectedStock.quantity.toString(), this.selectedStock.price.toString(), this.curTurn.toString()];
    this.data.apiCall('buy', dataArr , JSON.parse(localStorage.getItem('cipher_player'))['userID']).subscribe(
      data => {

        console.log(data);
        const res = data['res'];
        this.showSnackBar(res.status);
        this.loggedplayer.accountBalance -= this.totalPrice;
      });
  }

  onSearchStock(event: any) {
    this.txtSearch = event.target.value;
    if (this.txtSearch != null && this.txtSearch !== '') {
      this.stocks = this.transform(this.stocks, this.txtSearch);
    } else {
      this.stocks = this.dashboard.companies;
    }
  }

  transform(items: any[], criteria: any): any {

    return items.filter(item => {
      for (const key in item) {
        if (('' + item[key]).toLocaleLowerCase().includes(criteria.toLocaleLowerCase())) {
          return true;
        }
      }
      return false;
    });
  }
  //#endregion Stock

  //#region  History
  getHistory(id: string) {
    // get data from backend
    this.history = [];
    const purchase1 = new History();
    purchase1.sector = 'IT';
    purchase1.stock = 'Google';
    purchase1.price = 120;
    purchase1.quantity = 20;
    purchase1.transactionType = 'Sell';
    purchase1.turn = 1;
    purchase1.profit = 500;

    const purchase2 = new History();
    purchase2.sector = 'IT';
    purchase2.stock = 'Apple';
    purchase2.price = 200;
    purchase2.quantity = 10;
    purchase2.transactionType = 'Buy';
    purchase2.turn = 2;
    purchase2.profit = -20;

    this.history.push(purchase1);
    this.history.push(purchase2);
  }
  //#endregion History

  //#region Timer
  private startTimer() {

    const timer = Observable.timer(1, 1000);
    this.sub = timer.subscribe(
      t => {
        if (t > 3) {
          this.sub.unsubscribe();
          // load the dashboard again with next turn data
          // this.getDashboard();
        } else {
          this.ticks = t;

          this.secondsDisplay = this.getSeconds(this.ticks);
          this.minutesDisplay = this.getMinutes(this.ticks);
          this.hoursDisplay = this.getHours(this.ticks);
        }
      }
    );
  }

  private getSeconds(ticks: number) {
    return this.pad(ticks % 60);
  }

  private getMinutes(ticks: number) {
    return this.pad((Math.floor(ticks / 60)) % 60);
  }

  private getHours(ticks: number) {
    return this.pad(Math.floor((ticks / 60) / 60));
  }

  private pad(digit: any) {
    return digit <= 9 ? '0' + digit : digit;
  }
  //#endregion Timer

  showSnackBar(msg: string) {
    this.snackMsg = msg;
    const x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
  }

  getMyDetails(player:IPlayer){
    this.selectedPlayer = player;
    this.history = [];
    this.data.apiCall("getPlayerStaus",'no data',player.playerID).subscribe(
      data => {
        console.log(data);
        this.playerInfo = data['res'] as PlayerInfo;
        if(this.loggedplayer.playerID == player.playerID){
          this.loggedplayer.stocks = this.playerInfo.stockPortfolioList;
        }
        
        this.history = this.playerInfo.stockTransactions;
      });
    
  }

}
