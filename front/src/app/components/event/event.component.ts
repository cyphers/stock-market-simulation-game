import { Component, OnInit } from '@angular/core';

import {Event} from '../../shared/core/event';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {

  events:Event[]=[];

  constructor() { }

  ngOnInit() {
    this.getEvents();
  }

  getEvents(){
    //get backend data
    var event1 = new Event();
    event1.turnNo = 2;
    event1.evenType = "Sector";
    event1.eventName = "BOOM";

    var event2 = new Event();
    event2.turnNo = 3;
    event2.evenType = "Stock";
    event2.eventName = "PROFIT_WARNING";

    var event3 = new Event();
    event3.turnNo = 4;
    event3.evenType = "Sector";
    event3.eventName = "BUST";

    this.events.push(event1);
    this.events.push(event2);
    this.events.push(event3);
  }
}
