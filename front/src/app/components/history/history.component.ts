import { Component, OnInit, Input } from '@angular/core';

import {IPlayer} from '../../shared/core/player';
import {History} from '../../shared/core/history';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  @Input() player : IPlayer;
  history: History[] = [];

  constructor() { }

  ngOnInit() {

    this.getHistory(this.player.playerID);
  }

  getHistory(playerID:string){
    //get data from backend

    var purchase1 = new History();
    purchase1.sector="IT";
    purchase1.stock = "Google";
    purchase1.price = 120;
    purchase1.quantity = 20;
    purchase1.transactionType = "Sell";
    purchase1.turn = 1;
    purchase1.profit = 500;

    var purchase2 = new History();
    purchase2.sector="IT";
    purchase2.stock = "Apple";
    purchase2.price = 200;
    purchase2.quantity = 10;
    purchase2.transactionType = "Buy";
    purchase2.turn = 2;
    purchase2.profit = -20;

    this.history.push(purchase1);
    this.history.push(purchase2);
  }
}
