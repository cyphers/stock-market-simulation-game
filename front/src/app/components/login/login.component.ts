import { Component, OnInit } from '@angular/core';
import { DataService } from '../../data.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  gotLink: boolean;
  keyValue: string;
  userName: string;
  errorMsg: string;
  keys: any;
  players: any;
  valForProgress: number;
  progressBarText: string;
  progressBarPresentage: number;
  isGameJoinSuccess = false;
  gameCreator: string;

  creatorID: string;
  creatorName: string;
  userID: string;

  constructor(private data: DataService, private router: Router) {

    this.gotLink = false;
    this.errorMsg = '';
  }

  ngOnInit() {
  }

  setProgressBarWidth() {
    const val = this.valForProgress ? this.valForProgress : 0;
    this.progressBarPresentage = (val / (this.keys.length)) * 100;
    // tslint:disable-next-line:max-line-length
    this.progressBarText = (val === 0) ? '' : (val === 1) ? 'Only you are in the game still' : (val + ' of ' + (this.keys.length) + ' have joined the game.');
    const styles = {
      'width.%': this.progressBarPresentage
    };
    return styles;
  }

  onKey(event: any) {

    this.errorMsg = '';
    this.keyValue = event.target.value;
  }

  onUsername(event: any) {
    this.errorMsg = '';
    this.userName = event.target.value;
  }

  joinwithGame() {
    if (this.keyValue && this.userName) {
      console.log(this.keyValue, this.userName);
      this.askToJoin();
    } else {
      this.errorMsg = 'Please enter a valid Key and username';
    }
  }

  askToJoin() {
    console.log(this.keyValue, this.userName);
    this.data.apiCall('joinPlayerToAGame', this.userName, this.keyValue).subscribe(
      data => {
        console.log(data);
        const res: any = data['res'];
        if (res.status === 'Success') {
          this.isGameJoinSuccess = true;
          this.gameCreator = res.creatorName;
          this.players = res.playerList;
          this.creatorID = res.creatorID;
          this.userID = this.keyValue;
          this.getPlayerAddingStatus();
        } else {
          this.errorMsg = res.msg;
        }
      });
  }



  createNewGame() {
    if (this.userName) {
      console.log(this.userName);
      this.getKeys();

    } else {
      this.errorMsg = 'Please enter a valid username';
    }
  }

  getKeys() {
    console.log('calling to backend create a new game');
    this.data.apiCall('createNewGame', this.userName, '').subscribe(
      data => {
        console.log(data);
        this.keys = data['res'];
        this.creatorID = this.keys[0];
        this.userID = this.keys[0]; 
        this.gameCreator = this.userName;
        this.getPlayerAddingStatus();
      }

    );
  }

  getPlayerAddingStatus() {
    const getPlayerLoginStatus: any = setInterval(() => {
      this.data.apiCall('getPlayerAddedStatus', '', this.creatorID).subscribe(
        data => {
          console.log(data);
          if (new RegExp('START_GAME').test(data['res'])) { // check all keys taken by players. then start game
            localStorage.setItem('cipher_player', JSON.stringify({
              userName  : this.userName,
              userID    : this.userID
            }));
            clearInterval(getPlayerLoginStatus);



            this.router.navigate(['dashboard']);
          } else {
            if (new RegExp('Error').test(data['res'])) {
              if (!new RegExp('null').test(data['res'])) {
                this.errorMsg = data['res'];
                clearInterval(getPlayerLoginStatus);
              }
            } else {
              this.players = data['res']; // [ [key, playerName], [] , ...]
              if (data['res']) {
                this.valForProgress = this.players.length;
              }

              if (this.keys) {
                for (let j = 0; j < this.keys.length; j++) {
                  for (let i = 0; i < this.players.length; i++) {
                    if (this.players[i][0] === this.keys[j]) {
                      this.keys[j] = this.players[i][1] + ' has joined the game';
                    }
                  }
                }
              }


              // setTimeout(() => {
              //   clearInterval(getPlayerLoginStatus);
              //   this.errorMsg = 'Timeout : Please try again';
              //   this.clearGame(this.players[0][0]);
              //   setTimeout(() => {
              //     window.location.reload();
              //   }, 5000);
              // }, 1000 * 60 * 3);


            }
          }




        }
      );
    }, 7000);
  }

  clearGame(creatorKey) {
    this.data.apiCall('clearAGame', creatorKey, '').subscribe(
      data => {
        console.log(data);
      }
    );
  }

}
