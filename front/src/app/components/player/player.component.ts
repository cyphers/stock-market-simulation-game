import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { IPlayer } from '../../shared/core/player';
import { Stock } from '../../shared/core/stock';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {

  player: IPlayer;
  selectedStock:Stock;
  totalPrice:number;
  sellingQuantity:number; 
  isError:boolean;
  playerStock:Stock[] = [];
  @Output() bindPlayer: EventEmitter<IPlayer>  = new EventEmitter<IPlayer>();
  @Output() bindLoggedPlayer: EventEmitter<IPlayer>  = new EventEmitter<IPlayer>();

  constructor() { }

  ngOnInit() {

    //Have to get the data from back end
    //this.getLoggedPlayer();

    this.bindLoggedPlayer.emit(this.player);


    //initialize selected stock
    var selectedStock = new Stock();
    this.selectedStock = selectedStock;
  }

  getLoggedPlayer(){
    var player = new IPlayer();
    player.username = 'Alice';
    player.rank = 3;
    player.avatar = "../../../assets/images/female.jpg" ;
    player.accountBalance = 500.00;
    player.totalStock = 100;
    player.profit = 250;
    player.stocks = [];
    //player.stocks = this.getPlayerStock(name);
    this.player= player;
  }

  getPlayerStock(name:string){
    
    var stock1 = new Stock();
    stock1.sector = "IT";
    stock1.companyName = "Apple";
    stock1.price = 200;
    stock1.quantity = 50;
    this.playerStock.push(stock1);

    var stock2 = new Stock();
    stock2.sector = "IT";
    stock2.companyName = "Google";
    stock2.price = 150;
    stock2.quantity = 150;
    this.playerStock.push(stock2);
    return this.playerStock;
  }

  sellStock(stock:Stock){
    this.selectedStock = stock;
    this.sellingQuantity = null;
    this.totalPrice = 0;
  }

  onSetQuantity(event:any){
    var value = event.target.value;
    this.sellingQuantity = value;
    this.totalPrice = this.selectedStock.price * value;

    this.isError = (this.sellingQuantity > this.selectedStock.quantity) ? true : false; 
  }

  submitSellStock(){
     //pass stock to backend
  }

  showHistory(player:IPlayer){
    this.bindPlayer.emit(player);
  }
}
