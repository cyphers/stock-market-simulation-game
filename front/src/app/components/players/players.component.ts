import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { IPlayer } from '../../shared/core/player';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.css']
})
export class PlayersComponent implements OnInit {

  players: IPlayer[] = [];
  @Output() bindPlayer: EventEmitter<IPlayer>  = new EventEmitter<IPlayer>();

  constructor() { }

  ngOnInit() {

    //Have to get the data from back end
    this.getPlayers();
    
  }

  getPlayers(){
    var player1 = new IPlayer();
    player1.username = 'Tom';
    player1.rank = 1;
    player1.avatar = '../../../assets/images/male.png';
    this.players.push(player1);

    var player2 = new IPlayer();
    player2.username = 'AI player';
    player2.rank = 2;
    player2.avatar = "../../../assets/images/AI.png" ;
    this.players.push(player2);

    var player3 = new IPlayer();
    player3.username = 'Alice';
    player3.rank = 3;
    player3.avatar = "../../../assets/images/female.jpg" ;
    player3.isLogged = true;
    this.players.push(player3);

    var player4 = new IPlayer();
    player4.username = 'Bob';
    player4.rank = 4;
    player4.avatar = "../../../assets/images/male.png" ;
    this.players.push(player4);
  }

  showHistory(player:IPlayer){
    this.bindPlayer.emit(player);
  }
}
