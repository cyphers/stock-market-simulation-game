import { Component, OnInit, Input } from '@angular/core';

import {Stock} from '../../shared/core/stock'
import {IPlayer} from '../../shared/core/player'

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.scss']
})
export class StockComponent implements OnInit {

  @Input() loggedplayer:IPlayer;

  stocks:Stock[]=[];
  selectedStock:Stock;
  totalPrice:number;
  txtSearch:string;
  isError:boolean

  constructor() { }

  ngOnInit() {

    var selectedStock = new Stock();
    selectedStock.sector = "";
    selectedStock.companyName = "";
    selectedStock.price = 0;
    this.selectedStock = selectedStock;

    //have to take data from backend
    this.getStocks();
  }

  getStocks(){
    this.stocks = [];
    
    var stock1 = new Stock();
    stock1.sector = "IT";
    stock1.companyName = "Apple";
    stock1.price = 200;
    this.stocks.push(stock1);

    var stock2 = new Stock();
    stock2.sector = "IT";
    stock2.companyName = "Google";
    stock2.price = 150;
    this.stocks.push(stock2);

    var stock3 = new Stock();
    stock3.sector = "Finance";
    stock3.companyName = "ABC";
    stock3.price = 250;
    this.stocks.push(stock3);

    var stock4 = new Stock();
    stock4.sector = "IT";
    stock4.companyName = "Apple";
    stock4.price = 200;
    this.stocks.push(stock4);

    var stock5 = new Stock();
    stock5.sector = "IT";
    stock5.companyName = "Google";
    stock5.price = 150;
    this.stocks.push(stock5);

    var stock6 = new Stock();
    stock6.sector = "Finance";
    stock6.companyName = "ABC";
    stock6.price = 250;
    this.stocks.push(stock6);
  }

  buyStock(stock:Stock){
    
    this.selectedStock = stock;
    this.selectedStock.quantity = null;
    this.totalPrice = 0;
  }

  onSetQuantity(event:any){
    var value = event.target.value;
    this.selectedStock.quantity = value;
    this.totalPrice = this.selectedStock.price * value;

    this.isError = (this.loggedplayer.accountBalance < this.totalPrice) ? true : false; 
  }

  submitBuyStock(){
    //pass stock to backend
  }

  onSearchStock(event:any){
    this.txtSearch = event.target.value;
    if(this.txtSearch!=null && this.txtSearch != ""){
    this.stocks = this.transform(this.stocks,this.txtSearch);
    }
    else{
      this.getStocks();
    }
  }

  transform(items: any[], criteria: any): any {

    return items.filter(item => {
        for (let key in item) {
            if (("" + item[key]).toLocaleLowerCase().includes(criteria.toLocaleLowerCase())) {
                return true;
            }
        }
        return false;
    });
  }

}
