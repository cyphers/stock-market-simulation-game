import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//import * as jQuery from 'jquery';

@Injectable({
  providedIn: 'root'
})
export class DataService {


  url: string;
  proxy: string;
  params: any;


  constructor(private http: HttpClient) {
    this.url = 'https://l49tbyhov9.execute-api.us-east-2.amazonaws.com/Live';
    this.proxy = 'https://cryptic-headland-94862.herokuapp.com/';

  }



  apiCall(ctrl, data, userID) {
    this.params = {
      'controller': ctrl.toString(),
      'data': data,
      'userID': userID
    };

    console.log("backend call is going on", this.params);

    return this.http.post(this.proxy + this.url, JSON.stringify(this.params));
  }
}
