import {Stock} from './stock';
import {IPlayer} from './player';
import {Event} from './event';

export class Dashboard{
    companies: Stock[];
    chartInfo: number[];
    playerInfo: IPlayer[];
    eventPrediction?: string;
    analystRecommendations: string;
    gameStatus: boolean;
}