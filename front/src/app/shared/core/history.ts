export class History{
    turn:number;
    sector?:string;
    stock:string;
    quantity:number;
    price:number;
    transactionType:string;
    profit?:number;
}