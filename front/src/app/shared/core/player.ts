import {PlayerStock} from './playerStock'

export class IPlayer{
    playerID:string;
    username:string;
    rank?: number;
    avatar?: string;
    accountBalance?: number;
    profit?: number;
    totalStock?: number;
    isLogged?:boolean;
    stocks?:PlayerStock[]=[];
}