import {PlayerStock} from './playerStock';
import {History} from './history';

export class PlayerInfo{
    stockTransactions:History[] = [];
    stockPortfolioList:PlayerStock[] = [];
}