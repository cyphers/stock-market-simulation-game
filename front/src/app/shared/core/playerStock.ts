export class PlayerStock{
    sector?:string;
    company: string;
    quantity: number;
    purchasePrice: number;
}