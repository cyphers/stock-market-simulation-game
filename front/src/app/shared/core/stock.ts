export class Stock{
    sector:string;
    companyName:string;
    stockSymbol?:string
    price:number;
    quantity?:number;
}