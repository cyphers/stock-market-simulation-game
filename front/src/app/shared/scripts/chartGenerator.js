

function generateChart(data, options){
    google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      var newData = google.visualization.arrayToDataTable(data);

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(newData, options);
}